﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class TutorialScript : MonoBehaviour
{
    public GameObject tutorialText;
    public GameObject[] tutorialGameobjects;

    // Use this for initialization
    void Start()
    {

        Time.timeScale = 0;

    }

    // Update is called once per frame
    void Update()
    {
        

        if (Input.GetButton("Fire1"))
        {

            Time.timeScale = 1;
            gameObject.SetActive(false);
            tutorialText.SetActive(false);
            

        }
    }
}
