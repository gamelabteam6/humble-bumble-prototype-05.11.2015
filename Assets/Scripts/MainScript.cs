﻿using UnityEngine;
using System.Collections;

public class MainScript : MonoBehaviour {

    // thiese global variables will be set from the inspector.
    public GameObject stemObject;   // which prefab will represent the stem?
    public GameObject beeObject;   // which prefab will represent the bee?
    public GameObject flowerObject; // prefab for top of the flower.
    public GameObject spiderFangs; //prefab for fangs of the spider.
    public GameObject spiderWeakSpot; //prefab for Weak Spot of the spider.	
    public float pipeHole;          // how large is the gap between upper and lower pipes?

    // function to be executed once the script started
    void Start()
    {
        // placing the bee
        //Instantiate(birdObject);
        // calling "CreateObstacle" function after 0 seconds, then each 1.5 seconds, 
        InvokeRepeating("CreateObstacle", 0f, 1.5f);
        InvokeRepeating("CreateSpider", 0f, 3.0f);
    }

    void CreateSpider()
    {

        GameObject spiderKillsYou = Instantiate(spiderFangs);
        spiderKillsYou.transform.position = new Vector2(3.752f, -1.449f);
        GameObject youKillSpider = Instantiate(spiderWeakSpot);
        youKillSpider.transform.position = new Vector2(4.3f, -1.5f);


    }

    // function called by InvokeRepeating function
    void CreateObstacle()
    {
        // generating random upper pipe position
        float randomPos = 4f - (4f - 0.8f - pipeHole) * Random.value;
        // adding upper pipe to stage
        GameObject topFlower = Instantiate(flowerObject);
        // setting upper pipe position
        //upperPipe.transform.position = new Vector2(4f, randomPos);
        topFlower.transform.position = new Vector2 (4f, -2f);
        // adding lower pipe to stage
        GameObject lowerStem = Instantiate(stemObject);
        // setting lower pipe position
        //lowerPipe.transform.position = new Vector2(4f, randomPos - pipeHole - 4.8f);
        lowerStem.transform.position = new Vector2(4f, -2f);
      

    }
}
