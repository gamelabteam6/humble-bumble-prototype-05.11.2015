﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BeeScript : MonoBehaviour {

    // this global variable will be set from the inspector. Represents bee's jump force
    //public Vector2 jumpForce = new Vector2();
    // Beejump = how far down bee jumps, decreases by 1 each time it collides with flower to simulate bee getting heavier from nectar.
    public float beeJump;
    public Text scoreText;
    private float myScore;
    public Text livesText;
    private float lives;

   

    // function to be executed once the bee is created
    void Start()
    {

        //Setting Lives Float value
        lives = 3;
        //Setting livesText String Value
        livesText.text = "Lives: " + lives.ToString();
        
        // placing the bee
        transform.position = new Vector2(-2f, 0f);

        // Setting Score String value

        scoreText.text = "";
        myScore = 0;
    }

    // function to be executed at each frame
    void Update()
    {
        // waiting for mouse input
        if (Input.GetButton("Fire1"))
        {
            // setting bee's rigid body velocity to zero
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            // adding jump force to bee's rigid body
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * beeJump *Time.deltaTime);
        }
        // getting the real position, in pixels, of the bee on the stage
        Vector2 stagePos = Camera.main.WorldToScreenPoint(transform.position);
        // if the bee leaves the stage...
        if (stagePos.y > Screen.height || stagePos.y < 0)
        {
            // ... call die function
            die();
        }
    }

    // function to be executed once the bee enters in collision with anything
    void OnCollisionEnter2D(Collision2D col)
    {
        // call die function
        if (col.gameObject.tag == "Stem")
        {

            die();
            //myScore = myScore - 1;

        }

        if (col.gameObject.tag == "Top")
        {

            myScore = myScore + 3;
            scoreText.text = "Score: " + myScore.ToString();
            //GetComponent<Rigidbody2D>().gravityScale += 0.1f;
            beeJump = beeJump - 1;
            
            


        }

        if (col.gameObject.tag == "Fangs")
        {

            //die();
            lives = lives - 1;
            livesText.text = "Lives: " + lives.ToString();
            Destroy(GameObject.FindWithTag("Fangs"));
            Destroy(GameObject.FindWithTag("Weak Spot"));
            

        }
       
        if (lives == 0)
        {

            die();

        }


        if (col.gameObject.tag == "Weak Spot")
        {

            myScore = myScore + 1;
            scoreText.text = "Score: " + myScore.ToString();
            Destroy(GameObject.FindWithTag("Weak Spot"));
            Destroy(GameObject.FindWithTag("Fangs"));
        }
           
    }

    void die()
    {
        // reload the current scene - actually restart the game
        Application.LoadLevel(Application.loadedLevel);
    }
}
